#!/usr/bin/env bash

set -e

helm repo add bitnami https://charts.bitnami.com/bitnami
helm install mariadb bitnami/mariadb --values values.yaml
